<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        DROP TYPE IF EXISTS status_usuario;
        CREATE TYPE status_usuario AS ENUM (\'Apto\',
                       \'Inapto\');
        CREATE TABLE users
            (id bigserial NOT NULL,
             name VARCHAR(50) NOT NULL,
             cpf VARCHAR(11) NOT NULL,
             login VARCHAR(15) NOT NULL,
             password VARCHAR(15) NOT NULL,
             phone VARCHAR(14) NOT NULL,
             email VARCHAR(25) NOT NULL,
             status status_usuario NOT NULL,
             created_at timestamp NOT NULL DEFAULT now(),
             updated_at timestamp NOT NULL DEFAULT now(),
             CONSTRAINT user_pk PRIMARY KEY (id),
             CONSTRAINT cpf_uq UNIQUE (cpf),
             CONSTRAINT login_uq UNIQUE (login)
             );'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TABLE users;');
    }
}
