<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TABLE loans (
            id bigserial NOT NULL,
            loanDate DATE NOT NULL,
            devolutionDate DATE,
            typeLoan boolean NOT NULL,
            user_id bigint NOT NULL,
            book_id bigint NOT NULL,
            created_at timestamp NOT NULL DEFAULT now(),
            updated_at timestamp NOT NULL DEFAULT now(),
            CONSTRAINT loan_pk PRIMARY KEY (id),
            
            CONSTRAINT user_fk FOREIGN KEY (user_id)
                    REFERENCES users (id)
                    ON DELETE RESTRICT ON UPDATE CASCADE,
                    
            CONSTRAINT book_fk FOREIGN KEY (book_id)
                    REFERENCES books (id)
                    ON DELETE RESTRICT ON UPDATE CASCADE
        );
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TABLE loans;');
    }
}
