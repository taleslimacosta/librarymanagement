<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        DROP TYPE IF EXISTS status_livro;
        CREATE TYPE status_livro AS ENUM (\'Disponível\',
                                          \'Emprestado\');
        CREATE TABLE books (
            id bigserial NOT NULL,
            publishing_company VARCHAR(20) NOT NULL,
            edition INTEGER NOT NULL,
            title VARCHAR(20) NOT NULL,
            author VARCHAR(50) NOT NULL,
            publishing_date DATE NOT NULL,
            pages INTEGER NOT NULL,
            code VARCHAR(255) NULL,
            sector_id bigint NOT NULL,
            status status_livro NOT NULL DEFAULT \'Disponível\',
            created_at timestamp NOT NULL DEFAULT now(),
            updated_at timestamp NOT NULL DEFAULT now(),
            
            CONSTRAINT book_pk PRIMARY KEY (id),
            CONSTRAINT code_uq UNIQUE (code),
            
            CONSTRAINT sector_fk FOREIGN KEY (sector_id)
                    REFERENCES sectors (id)
                    ON DELETE RESTRICT ON UPDATE CASCADE
        );
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TABLE books;');
    }
}
