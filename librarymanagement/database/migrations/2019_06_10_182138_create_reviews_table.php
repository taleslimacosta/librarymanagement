<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TABLE reviews (
            id bigserial NOT NULL,
            reason VARCHAR(255) NOT NULL,
            loan_id bigint NOT NULL,
            created_at timestamp NOT NULL DEFAULT now(),
            updated_at timestamp NOT NULL DEFAULT now(),
            CONSTRAINT review_pk PRIMARY KEY (id),
            
            CONSTRAINT loan_fk FOREIGN KEY (loan_id)
                    REFERENCES loans (id)
                    ON DELETE RESTRICT ON UPDATE CASCADE
        );
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TABLE reviews;');
    }
}
