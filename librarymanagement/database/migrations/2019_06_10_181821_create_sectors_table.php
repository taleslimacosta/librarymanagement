<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        DROP TYPE IF EXISTS categoria_livro;
        CREATE TYPE categoria_livro AS ENUM (\'Humanas\',
                                           \'Exatas\',
                                           \'Biologicas\');
        CREATE TABLE sectors (
            id bigserial NOT NULL,
            floor INTEGER NOT NULL,
            category categoria_livro NOT NULL,
            employee_id bigint NOT NULL,
            created_at timestamp NOT NULL DEFAULT now(),
            updated_at timestamp NOT NULL DEFAULT now(),
            CONSTRAINT sector_pk PRIMARY KEY (id),
            
            CONSTRAINT employee_fk FOREIGN KEY (employee_id)
                    REFERENCES employees (id)
                    ON DELETE RESTRICT ON UPDATE CASCADE
        );
        
        INSERT INTO sectors(floor,category,employee_id,created_at,updated_at) 
        VALUES (1,\'Humanas\',1,DEFAULT,DEFAULT);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TABLE sectors;');
    }
}
