<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        DROP TYPE IF EXISTS cargo_funcionario;
        CREATE TYPE cargo_funcionario AS ENUM (\'Administrador de Conteudo\',
                       \'Bibliotecario\',
                       \'Balconista de Empréstimo\',
                       \'Bibliotecário Chefe\');
        CREATE TABLE employees
            (id bigserial NOT NULL,
             name VARCHAR(50) NOT NULL,
             cpf VARCHAR(14) NOT NULL,
             password VARCHAR(15) NOT NULL,
             office cargo_funcionario NOT NULL,
             registration VARCHAR(10) NOT NULL,
             phone VARCHAR(11) NOT NULL,
             email VARCHAR(25) NOT NULL,
             created_at timestamp NOT NULL DEFAULT now(),
             updated_at timestamp NOT NULL DEFAULT now(),
             CONSTRAINT employee_pk PRIMARY KEY (id),
             CONSTRAINT registration_uq UNIQUE (registration)
             );
             
        INSERT INTO employees(name,cpf,password,office,registration,phone,email,created_at,updated_at) VALUES 
        (\'Joãozinho\',\'101.101.101-10\',\'111\',\'Administrador de Conteudo\',\'201906101\',\'88888888888\',\'aaaaaaaaaaa@atena.edu.br\',DEFAULT,DEFAULT);'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TABLE employees;');
    }
}
