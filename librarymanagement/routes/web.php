<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/books', 'BookController@index')->name('books.index');
Route::post('/books', 'BookController@store')->name('books.store');
Route::get('/books/create', 'BookController@create')->name('books.create');
Route::patch('/books/{book}', 'BookController@update')->name('books.update');
Route::delete('/books/{book}', 'BookController@destroy')->name('books.destroy');
Route::get('/books/{book}', 'BookController@edit')->name('books.edit');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
