@extends('layout')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="card uper">
        <div class="card-header" style="text-align: center;">
            Adicionar Livro
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('books.store') }}">
                @method('POST')
                @csrf
                <div class="form-group">
                    <label for="title">Titulo:</label>
                    <input type="text" class="form-control" name="title"/>
                </div>
                <div class="form-group">
                    <label for="author">Autor (a):</label>
                    <input type="text" class="form-control" name="author"/>
                </div>
                <div class="form-group">
                    <label for="publishing_company">Editora:</label>
                    <input type="text" class="form-control" name="publishing_company"/>
                </div>
                <div class="form-group">
                    <label for="edition">Edição:</label>
                    <input type="text" class="form-control" name="edition"/>
                </div>
                <div class="form-group">
                    <label for="publishing_date">Data de Publicação:</label>
                    <input type="date" class="form-control" name="publishing_date"/>
                </div>
                <div class="form-group">
                    <label for="pages">Páginas:</label>
                    <input type="text" class="form-control" name="pages"/>
                </div>
                <div class="form-group">
                    <label for="sector_id">Setor:</label>
                    <input type="text" class="form-control" name="sector_id"/>
                </div>
                <div class="float-right">
                    <button type="submit" style="" class="btn btn-primary">Enviar</button>
                </div>
            </form>
        </div>
    </div>
@endsection
