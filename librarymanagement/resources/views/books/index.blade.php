@extends('layout')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="uper">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br />
        @endif
            <td><a href="{{ route('books.create')}}" class="btn btn-primary btn-lg btn-block">Inserir Livro</a></td>
        <table class="table table-striped">
            <thead>
            <tr>
                <td style="text-align: center;">ID</td>
                <td style="text-align: center;">Titulo</td>
                <td style="text-align: center;">Autor (a)</td>
                <td style="text-align: center;">Editora</td>
                <td style="text-align: center;">Edição</td>
                <td style="text-align: center;">Data de Publicação</td>
                <td style="text-align: center;">Páginas</td>
                <td style="text-align: center;">Código</td>
                <td style="text-align: center;">Status</td>
                <td style="text-align: center; vertical-align: middle;" colspan="2">Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($books as $book)
                <tr>
                    <td style="text-align: center; vertical-align: middle;">{{$book->id}}</td>
                    <td style="text-align: center; vertical-align: middle;">{{$book->title}}</td>
                    <td style="text-align: center; vertical-align: middle;">{{$book->author}}</td>
                    <td style="text-align: center; vertical-align: middle;">{{$book->publishing_company}}</td>
                    <td style="text-align: center; vertical-align: middle;">{{$book->edition}}</td>
                    <td style="text-align: center; vertical-align: middle;">{{$book->publishing_date}}</td>
                    <td style="text-align: center; vertical-align: middle;">{{$book->pages}}</td>
                    <td style="text-align: center; vertical-align: middle;">{{$book->code}}</td>
                    <td style="text-align: center;">
                        <span class="badge badge-pill badge-success" style="padding: 10px;">
                            {{$book->status}}
                        </span>

                    </td>
                    <td style="text-align: center; vertical-align: middle;"><a href="{{ route('books.edit',$book->id)}}" class="btn btn-dark">Editar</a></td>
                    <td style="text-align: center; vertical-align: middle;">
                        <form action="{{ route('books.destroy', $book->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Deletar</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div>
@endsection
