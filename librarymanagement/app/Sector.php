<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Sector extends Model
{
    protected $fillable = ['floor', 'category', 'employee_id'];

    public function books(): HasMany {
        return $this->hasMany(Book::class);
    }
}
