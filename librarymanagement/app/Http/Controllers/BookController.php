<?php

namespace App\Http\Controllers;

use App\Book;
use App\Sector;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();

        return view('books.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.create');
    }

    public function codeGenerator(Sector $sector, $number, $id)
    {
        $string = $sector->floor.'.'.$sector->id;
        if($sector->category == 'Exatas') {
            $string = $string.'.E';
        } elseif ($sector->category == 'Humanas') {
            $string = $string.'.H';
        } elseif ($sector->category == 'Biologicas') {
            $string = $string.'.B';
        }
        return $string.".".$number.".".$id;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'publishing_company' => 'required|string|max:20',
            'edition' => 'required|integer',
            'title' => 'required|string|max:20',
            'author' => 'required|string|max:50',
            'publishing_date' => 'required|date',
            'pages' => 'required|integer',
            'sector_id' => 'required|integer|exists:sectors,id']);

        $book = new Book([
            'publishing_company' => $request->get('publishing_company'),
            'edition' => $request->get('edition'),
            'title' => $request->get('title'),
            'author' => $request->get('author'),
            'publishing_date' => $request->get('publishing_date'),
            'pages' => $request->get('pages'),
            'sector_id' => $request->get('sector_id'),
            'status' => 'Disponível'
        ]);

        $book->save();

        $book->update(['code' => $this->codeGenerator(Sector::find($book->sector_id),$book->edition,$book->id)]);

        return redirect('books')->with('success', 'Book added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);

        return view('books.edit', compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'publishing_company' => 'required|string|max:20',
            'edition' => 'required|integer',
            'title' => 'required|string|max:20',
            'author' => 'required|string|max:50',
            'publishing_date' => 'required|date',
            'pages' => 'required|integer',
            'sector_id' => 'required|integer|exists:sectors,id']);

        $book = Book::find($id);

        $book->update([
            'publishing_company' => $request->get('publishing_company'),
            'edition' => $request->get('edition'),
            'title' => $request->get('title'),
            'author' => $request->get('author'),
            'publishing_date' => $request->get('publishing_date'),
            'pages' => $request->get('pages'),
            'sector_id' => $request->get('sector_id')
        ]);

        $book->save();

        $book->update(['code' => $this->codeGenerator(Sector::find($book->sector_id),$book->edition,$book->id)]);

        return redirect('/books')->with('success', 'Book has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();

        return redirect('/books')->with('success', 'Book has been deleted Successfully');
    }
}
