<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Book extends Model
{
    protected $fillable = [
        'publishing_company',
        'edition',
        'title',
        'author',
        'publishing_date',
        'pages',
        'code',
        'sector_id',
        'status'
    ];

    public function sectors(): HasOne {
        return $this->hasOne(Sector::class);
    }
}
